# Yii2 Yandex Maps Widget

Supports Markers and Clusters now.

### Install

Either run

```
$ php composer.phar require stalkerrr/yii2-yandex-map "*"
```

or add

```
"stalkerrr/yii2-yandex-map": "*"
```

to the ```require``` section of your `composer.json` file.

## Usage

### How to call?
```php
	// add this in your view
	
	use stalkerrr\yandex_map\YandexMapWidget;
	use stalkerrr\yandex_map\YandexPreset;
	use stalkerrr\yandex_map\YandexMapCluster;
	
	
	//create preset object with params of geoMarker (like color, type and other)
	$testPreset = new YandexPreset();
	$testPreset->type = YandexPreset::TYPE_DOT;
    $testPreset->color = YandexPreset::COLOR_DARK_ORANGE;
    //$testPreset->icon = YandexPreset::ICON_MONEY;
    $testPreset->circle = true;
	
	//simple way calling
	$mapWidget = YandexMapWidget::widget(
		[
			'points' => [
        		['coord' => [53.21, 32.34], 'content' => [], 'opts' => ['preset' => $testPreset]],
        		['coord' => [54.21, 30.34]]
			]
        ]
    );
```

### Creating clusters or collections dynamically
```php
	$testPreset = new YandexPreset();
	$testPreset->type = YandexPreset::TYPE_DOT;
	$testPreset->color = YandexPreset::COLOR_DARK_ORANGE;
	$testPreset->circle = true;

    $widget = YandexMapWidget::begin(
        ['points' => [
            ['coord' => [53.21, 32.34], 'content' => [], 'opts' => ['preset' => $testPreset]],
            ['coord' => [54.21, 30.34]]]
        ]
    );
    $cluster = new YandexMapCluster();
    $cluster->preset = $testPreset;

    $testPreset->color=YandexPreset::COLOR_BLACK;
    $widget->addCluster(
        [
            ['coord' => [43.21, 31.34],'content' => [], 'opts' => ['preset' => $testPreset]],
            ['coord' => [45.31, 31.34]]
        ],
        $cluster
     );


	$widget->addCollection([['coord' => [32.25, 21.54]], ['coord' => [40.21, 39.14]]], $testPreset);
    
    YandexMapWidget::end();
	
```

## Built With

* [YandexMapApi](https://tech.yandex.ru/maps/) - Api for Yandex Maps display

## Authors

* **David Khachatryan** - *Initial work* - [stalkerrr](https://bitbucket.org/stalkerrr/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

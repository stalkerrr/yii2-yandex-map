<?php
namespace stalkerrr\yandex_map;
use yii\web\AssetBundle;

class YandexMapAsset extends AssetBundle
{
    public $js = [
        'https://api-maps.yandex.ru/2.1/?lang=ru_RU'
    ];
}

<?php
namespace stalkerrr\yandex_map;

use yii\base\Widget;
use Yii;

class YandexMapWidget extends Widget
{
    const CONTROL_ZOOM = 'zoomControl';
    const CONTROL_FULL_SCREEN = 'fullscreenControl';
    const CONTROL_RULER = 'rulerControl';
    const CONTROL_GEOLOCATION = 'geolocationControl';
    const CONTROL_SEARCH = 'searchControl';

    const BEHAV_DEFAULT = 'default';
    const BEHAV_DRAG = 'drag';
    const BEHAV_SCROLL_ZOOM = 'scrollZoom';
    const BEHAV_DBL_CLICK_ZOOM = 'dblClickZoom';
    const BEHAV_MULTI_TOUCH = 'multiTouch';
    const BEHAV_RMB_MAGNIFIER = 'rightMouseButtonMagnifier';
    const BEHAV_LMB_MAGNIFIER = 'leftMouseButtonMagnifier';
    const BEHAV_ROUTE_EDITOR = 'routeEditor';
    const BEHAV_RULER = 'ruler';

    const SEARCH_PROVIDER_YA = 'yandex#search';

    public $points = [];
    public $preset;
    public $id;
    public $controls = [self::CONTROL_ZOOM, self::CONTROL_FULL_SCREEN];
    public $center = [55.751574, 37.573856];
    public $behaviors = [self::BEHAV_DEFAULT, self::BEHAV_SCROLL_ZOOM];
    public $searchControllerProvider = self::SEARCH_PROVIDER_YA;

    protected $zoom = 2;
    private $zoomValues = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    public function setZoomValue(int $zoom){
        $this->zoom = in_array($zoom, $this->zoomValues) ? $zoom : 2;
    }

    protected $cluster = null;
    public function setClusterer(YandexMapCluster $cluster)
    {
        $this->cluster = $cluster;
    }

    protected $collections = [];
    public function addCollection(array $points, $preset = null)
    {
        if (empty($preset)){
            $preset = (string)(new YandexPreset());
        }
        array_push($this->collections, json_encode(compact('points', 'preset')));
    }
    public function addCluster(array $points, YandexMapCluster $clusterer)
    {
        $clusterer->preset->forCluster = true;
        $cluster = $this->clusterOptions($clusterer);
        array_push($this->collections, json_encode(compact('points', 'cluster')));
        $clusterer->preset->forCluster = false;
    }
    protected function getCollectionForJs()
    {
        return '['.implode(',',$this->collections) . ']';
    }

    public function init()
    {
        YandexMapAsset::register($this->view);

        if ($this->cluster){
            $this->addCluster($this->points, $this->cluster);
        }
        else{
            $this->addCollection($this->points, $this->preset);
        }
        parent::init();
    }

    public function run()
    {
        $mapOpts = $this->jsMapOptions();
        $mapSearchOpts = $this->jsMapSearchOptions();

        $collections = $this->getCollectionForJs();
        $id = $this->id ?? Yii::$app->getSecurity()->generateRandomString(6);

        return $this->render(
            'map',
            compact('mapOpts', 'mapSearchOpts', 'collections', 'id')
        );
    }

    protected function jsMapOptions()
    {
        $options = [
            'center' => $this->center,
            'zoom' => $this->zoom,
            'controls' => $this->controls,
            'behaviors' => $this->behaviors
        ];
        return json_encode($options);
    }

    protected function jsMapSearchOptions()
    {
        return json_encode(
            [
                'searchControlProvider' => $this->searchControllerProvider
            ]
        );
    }

    protected function clusterOptions($cluster)
    {
        if ($cluster){
            return [
                'preset' => $cluster->preset,
                'groupByCoordinates' => $cluster->groupByCoordinates,
                'clusterHideIconOnBalloonOpen' => $cluster->clusterHideIconOnBalloonOpen,
                'geoObjectHideIconOnBalloonOpen' => $cluster->geoObjectHideIconOnBalloonOpen,
                'gridSize' => $cluster->gridSize,
                'clusterDisableClickZoom' => $cluster->clusterDisableClickZoom,
                'preset' => (string) $cluster->preset
            ];
        }
        return null;
    }
}

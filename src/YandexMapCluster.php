<?php
namespace stalkerrr\yandex_map;

use yii\base\Model;

class YandexMapCluster extends Model
{
    public $gridSize = 80;
    public $clusterDisableClickZoom = false;
    public $groupByCoordinates = false;

    protected $_preset;
    public function setPreset(YandexPreset $preset)
    {
        $preset->forCluster = true;
        $this->_preset = $preset;

    }
    public function getPreset()
    {
        return $this->_preset;
    }

    public $clusterHideIconOnBalloonOpen = false;
    public $geoObjectHideIconOnBalloonOpen = false;
}
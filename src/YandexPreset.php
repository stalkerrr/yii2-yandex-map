<?php

namespace stalkerrr\yandex_map;

use yii\base\Model;

class YandexPreset extends Model implements \JsonSerializable
{
    const CIRCLE = 'Circle';
    const PREFIX = 'islands#';
    const POSTFIX_CLUSTER = 'ClusterIcons';

    const TYPE_STRETCHY = 'StretchyIcon';
    const TYPE_SIMPLE = 'Icon';
    const TYPE_DOT = 'DotIcon';

    const ICON_MONEY = 'Money';

    const COLOR_BLUE = 'blue';
    const COLOR_RED = 'red';
    const COLOR_DARK_ORANGE = 'darkOrange';
    const COLOR_NIGHT = 'night';
    const COLOR_DARK_BLUE = 'darkBlue';
    const COLOR_PINK = 'pink';
    const COLOR_GRAY = 'gray';
    const COLOR_BROWN = 'brown';
    const COLOR_DARK_GREEN = 'darkGreen';
    const COLOR_VIOLET = 'violet';
    const COLOR_BLACK = 'black';
    const COLOR_YELLOW = 'yellow';
    const COLOR_GREEN = 'green';
    const COLOR_ORANGE = 'orange';
    const COLOR_LIGHT_BLUE = 'lightBlue';
    const COLOR_OLIVE = 'olive';

    public $color = self::COLOR_GRAY;
    public $type = self::TYPE_SIMPLE;
    public $icon = null;
    public $circle = false;

    public $forCluster = false;

    public function __toString()
    {
        $start = self::PREFIX . $this->color;
        $end = '';

        if ($this->icon){
            $end = $this->icon . ($this->circle ? self::CIRCLE : '') . self::TYPE_SIMPLE;

        }
        elseif ($this->forCluster){
            $end = self::POSTFIX_CLUSTER;
        }
        else{
            $end = ($this->circle ? self::CIRCLE : '') . $this->type;
        }


        return $start . $end;
    }

    public function jsonSerialize()
    {
        return $this->__toString();
    }
}
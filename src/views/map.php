<?php
/**
 * @var $mapOpts string
 * @var $mapSearchOpts string
 * @var $points string
 * @var $clusterOpts string
 *
 */
?>
    <div style="min-height:400px;" id="<?= $id?>"></div>
<?php
$js = <<< JS
ymaps.ready(function () {
    var myMap = new ymaps.Map('{$id}', {$mapOpts}, {$mapSearchOpts});
    var collections = {$collections};

    for(var i = 0, len = collections.length; i < len; i++) {
        var points = collections[i]['points'];
        if (points.length > 0){
            if (!collections[i]['cluster']){
                var markersCollection = new ymaps.GeoObjectCollection({}, {'preset': collections[i]['preset'] });
                for(var j = 0, pointsLen = points.length; j < pointsLen; j++) {
                    markersCollection.add(
                        new ymaps.Placemark(points[j]['coord'], points[j]['content'], points[j]['opts'])
                        );
                }
                myMap.geoObjects.add(markersCollection);
            }
            else {
                var geoObjec = [];
                for(var k = 0, leng = points.length; k < leng; k++) {
           
                    geoObjec[k] = new ymaps.Placemark(points[k]['coord'], points[k]['content'], points[k]['opts']);
                }

                var clusterer = new ymaps.Clusterer(collections[i]['cluster']);   
                
                    clusterer.add(geoObjec);
                    myMap.geoObjects.add(clusterer);
                    // myMap.setBounds(clusterer.getBounds(), {
                    //     checkZoomRange: true
                    // });
            }
        }
    }
});
JS;

$this->registerJs($js);
?>
